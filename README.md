# String Plus
An extension to the build-in String object
___

## USAGE

```javascript
// At your entry point (index.js || app.js)
require('@uzelux/string');
```
### Capitalize
 
<details>
<summary>

Capitalize the first character

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.capitalize();
// 'A quick brown fox jumps over lazy dog'
```

</details>
  
### ToCamelCase 

<details>
<summary>

Returns the calling string value converted to camelCase.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.toCamelCase();
// 'theQuickBrownFoxJumpsOverLazyDog'
```

</details>

### ToPascalCase 
  
<details>
<summary>

Returns the calling string value converted to PascalCase.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.toPascalCase();
// 'TheQuickBrownFoxJumpsOverLazyDog'
```

</details>
  
### ToSnakeCase 

<details>
<summary>

Returns the calling string value converted to snake_case.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.toSnakeCase();
// 'the_quick_brown_fox_jumps_over_lazy_dog'
```

</details>

### ToKebabCase 
  
<details>
<summary>

Returns the calling string value converted to kebab-case.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.toKebabCase();
// 'the-quick-brown-fox-jumps-over-lazy-dog'
```

</details>

### ToBinary 
  
<details>
<summary>

Returns the calling string value converted to binary string.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.toBinary();
// '1110100 1101000 1100101 100000 1110001 1110101 1101001 1100011 1101011 100000 1100010 1110010 1101111 1110111 1101110 100000 1100110 1101111 1111000 100000 1101010 1110101 1101101 1110000 1110011 100000 1101111 1110110 1100101 1110010 100000 1101100 1100001 1111010 1111001 100000 1100100 1101111 1100111'

```

</details>

### Truncate 
  
<details>
<summary>

Extract a string to specified length and returns it as a new string, without modifying the original string.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.truncate(5);
// 'the q'
```

</details>

### Reverse 
  
<details>
<summary>

Reverse a string and returns it as a new string, without modifying the original string.

</summary>

```javascript
'the quick brown fox jumps over lazy dog'.reverse();
// 'god yzal revo spmuj xof nworb kciuq eht'
```

</details>

### IsString 
  
<details>
<summary>

Check if an variable is a String object instance or a string

</summary>

```javascript
String.isString('the quick brown fox jumps over lazy dog');
// true
String.isString({})
// false
```

</details>
