/**
 * @return {string}
 */
String.prototype.capitalize = function () {return this.slice().replace(/(.)/, (match) => match.toUpperCase())};

/**
 * @return {string}
 */
String.prototype.toCamelCase = function () {
  return this.slice()
    .replace(/(\s.)/g, (match) => match.toUpperCase().trim());
};

/**
 * @return {string}
 */
String.prototype.toPascalCase = function () {
  return this.slice()
    .replace(/^.|(\s.)/g, (match) => match.toUpperCase().trim());
};


/**
 * @return {string}
 */
String.prototype.toSnakeCase = function () {return this.slice().replace(/\s/g, '_').toLowerCase();};


/**
 * @return {string}
 */
String.prototype.toKebabCase = function () {return this.slice().replace(/\s/g, '-').toLowerCase();};


/**
 * @param {number} length
 * @return {string}
 */
String.prototype.truncate = function (length) {return this.slice(0, length)};


/**
 * @return {string}
 */
String.prototype.toBinary = function () {
  return this.split('')
    .reduce((o, c) => `${o} ${c.charCodeAt(0).toString(2)}`, '')
    .trim();
};


/**
 * @return {string}
 */
String.prototype.reverse = function () {return this.split('').reverse().join('')};


/**
 * @param {*} input
 * @return {string}
 */
String.isString = (input) => input.constructor.name === 'String' || input instanceof String;
